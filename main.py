import argparse
import ast

if __name__ == '__main__':
    parser = argparse.ArgumentParser()
    parser.add_argument("-f", "--file", type=str, required=True)
    args = parser.parse_args()
    with open(args.file, "r") as f:
        title = None
        temperature = None
        vitesse = None
        for line in f:
            line: str
            if title is None:
                title = line
            elif temperature is None:
                temperature = ast.literal_eval(line[len("Temperature : "): -1])
            elif vitesse is None:
                vitesse = ast.literal_eval(line[len("Vitesse : "): -1])
            else:
                title = None
                temperature = None
                vitesse = None
                continue

            if title is not None and temperature is not None and vitesse is not None:
                print(title)
                print("[")
                print(f"  SoundVelocityPoint(temperature: -273, velocity: {vitesse[0]}),")
                for i in range(0, len(temperature)):
                    print(f"  SoundVelocityPoint(temperature: {temperature[i]}, velocity: {vitesse[i]}),")
                print("],")
